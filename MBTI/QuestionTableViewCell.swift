//
//  QuestionTableViewCell.swift
//  MBTI
//
//  Created by deadbird on 29/12/2017.
//  Copyright © 2017 deadbird. All rights reserved.
//

import UIKit

class QuestionTableViewCell: UITableViewCell {
    @IBOutlet weak var questionTitle: UILabel!
    @IBOutlet weak var answer1Button: QuestionButton!
    @IBOutlet weak var answer2Button: QuestionButton!
    @IBOutlet weak var imageForTitle: UIImageView!
	
	var cellIndex = 0 {
		didSet {
			answer1Button.questionIndex = cellIndex
			answer2Button.questionIndex = cellIndex
		}
	}
	
	private let unselectedColor = UIColor(rgb: 0xFFFFFF)
	private let selectedColor = UIColor(rgb: 0xEEEEEE)

    override func awakeFromNib() {
        super.awakeFromNib()		
    }
    
    public func updateSelection(selectedAnswer:SelectedAnswer?) {
        switch selectedAnswer {
        case nil:
            noAnswerSelected()
        case .answerA?:
            answer1Selected(UIButton())
        case .answerB?:
            answer2Selected(UIButton())
        }
    }
	
    public func noAnswerSelected() {
        answer1Button.backgroundColor = unselectedColor
        answer2Button.backgroundColor = unselectedColor
    }

	@IBAction func answer1Selected(_ sender: UIButton) {
		answer1Button.backgroundColor = selectedColor
		answer2Button.backgroundColor = unselectedColor
	}
	
	@IBAction func answer2Selected(_ sender: UIButton) {
		answer2Button.backgroundColor = selectedColor
		answer1Button.backgroundColor = unselectedColor
	}
}
