//
//  ViewController.swift
//  MBTI
//
//  Created by deadbird on 25/12/2017.
//  Copyright © 2017 deadbird. All rights reserved.
//

import UIKit
import Foundation
import Hue

extension UIColor {
	convenience init(red: Int, green: Int, blue: Int) {
		assert(red >= 0 && red <= 255, "Invalid red component")
		assert(green >= 0 && green <= 255, "Invalid green component")
		assert(blue >= 0 && blue <= 255, "Invalid blue component")
		
		self.init(red: CGFloat(red) / 255.0, green: CGFloat(green) / 255.0, blue: CGFloat(blue) / 255.0, alpha: 1.0)
	}
	
	convenience init(rgb: Int) {
		self.init(
			red: (rgb >> 16) & 0xFF,
			green: (rgb >> 8) & 0xFF,
			blue: rgb & 0xFF
		)
	}
}

struct IntroElements {
	var image:String = ""
	var bgColor:UIColor = UIColor.white
	var text:String = ""
}

class IntroViewController: UIViewController {

    @IBOutlet weak var image: UIImageView!
	@IBOutlet weak var text: UILabel!
	@IBOutlet var swipeGestureRecognizer: UISwipeGestureRecognizer!
	@IBOutlet weak var pageCounter: UIPageControl!
	
	private var animationNumber:Int = 0 {
		willSet {
		}
		didSet {
			if animationNumber >= introElementsList.count {
				animationNumber = introElementsList.count - 1
			}
			if animationNumber < 0 {
				animationNumber = 0
			}
			pageCounter.currentPage = animationNumber
			animateChanges()
		}
	}
	private let animationDuration = 0.4

	private let introElementsList:[IntroElements] = [
		IntroElements(image: "transfer", 		bgColor: UIColor(rgb:0x113F67), text: "L'indicateur typologique de Myers-Briggs (MBTI) est directement inspiré de la théorie des \"types psychologiques\" du psychologue suisse, C.G. JUNG. Il a été élaboré au cours de 40 années de recherches et d'expérimentations par K. Briggs et I. Myers et continue de faire l'objet de recherches et d'améliorations."),
		IntroElements(image: "seo-1", 			bgColor: UIColor(rgb:0x144876), text: "Son principe fondamental est le suivant : les variations de comportement que l'on observe entre les individus ne sont pas le résultat du hasard mais la conséquence de préférences spontanées concernant quatre dimensions principales dont chacune est caractérisée par deux pôles opposés"),
		IntroElements(image: "analytics", 		bgColor: UIColor(rgb:0x165185), text: "Orientation de l'énergie vers:\nE - Extraversion (le monde extérieur, les personnes, les choses)\nI - Introversion (le monde intérieur, les idées, les états internes)"),
		IntroElements(image: "chat2", 			bgColor: UIColor(rgb:0x195B94), text: "Modalités de perception:\nS - Sensation (démarche analytique, intérêt pour les faits, le présent)\nN - Intuition (démarche globale, intérêt pour les possibilités, le futur)"),
		IntroElements(image: "team-2", 			bgColor: UIColor(rgb:0x306B9E), text: "Critères de décision:\nT - Pensée/Thinking (logique, objectivité, application de principes)\nF - Sentiment/Feeling (valeurs, subjectivité, intérêt pour les personnes)"),
		IntroElements(image: "chat", 			bgColor: UIColor(rgb:0x477BA9), text: "Style de vie:\nJ - Jugement (organisation, méthode, planification)\nP - Perception (réactivité, spontanéité, adaptabilité)"),
		IntroElements(image: "networking-1", 	bgColor: UIColor(rgb:0x5E8CB4), text: "La combinaison des préférences sur ces 4 dimensions conduit à définir 16 \"types\", qui peuvent être regroupés eux-mêmes en 4 grandes familles, les \"tempéraments\". Chaque type est caractérisé par une dynamique d'évolution particulière. Il n'y a pas de bon ou de mauvais type, chacun a ses caractéristiques propres."),
		IntroElements(image: "teamwork-2", 		bgColor: UIColor(rgb:0x759CBE), text: "Vous seul pouvez dire quelles sont vos préférences et quel est votre type. Le questionnaire est un auxiliaire précieux mais, en dernier ressort, c'est vous qui êtes seul juge de ce que vous êtes et de ce que vous voulez devenir."),
		IntroElements(image: "curriculum-1", 	bgColor: UIColor(rgb:0x8CADC9), text: "Le MBTI a pour but de vous aider à mieux vous connaître et à mieux vous développer. Il vous permet également de mieux comprendre les autres, de communiquer plus facilement et de faire des différences une source d'enrichissement mutuel."),
		IntroElements(image: "laptop", 			bgColor: UIColor(rgb:0xA3BDD4), text: "Mode d'emploi : complétez l'onglet \"Questionnaire\" en choisissant la réponse qui vous correspond le mieux. Une fois le questionnaire complet, l'application vous donnera votre profil."),
		IntroElements(image: "certificate", 	bgColor: UIColor(rgb:0xBACDDE), text: "Prêt?"),
	]
	
	override func viewWillAppear(_ animated: Bool) {
		image.image = UIImage(named: introElementsList[animationNumber].image)
		view.backgroundColor = introElementsList[animationNumber].bgColor
		text.text = introElementsList[animationNumber].text
		pageCounter.numberOfPages = introElementsList.count
		pageCounter.currentPage = 0
	}
	
	@IBAction func skipIntro(_ sender: UIButton) {
		
	}
	@IBAction func swipeRight(_ sender: UISwipeGestureRecognizer) {
		print("Swipe right, animationNumber=\(animationNumber)")
		animationNumber -= 1
	}
	
	@IBAction func swipeLeft(_ sender: UISwipeGestureRecognizer) {
		print("Swipe left, animationNumber=\(animationNumber)")
		animationNumber += 1
	}
	
	private func animateChanges() {
        UIView.transition(with: self.image, duration: animationDuration, options: .transitionCrossDissolve, animations: {
            self.image.image = UIImage(named: self.introElementsList[self.animationNumber].image)
        })
        UIView.animate(withDuration: animationDuration, animations: {
            self.view.backgroundColor = self.introElementsList[self.animationNumber].bgColor
        })
		UIView.animate(withDuration: animationDuration/2, animations: {
			self.text.alpha = 0
		}, completion: setNewText)
    }
	
	private func setNewText(_:Bool) -> Void {
		text.text = introElementsList[animationNumber].text
		
		if animationNumber == introElementsList.count - 1 {
			text.font = text.font.withSize(45)
		} else {
			text.font = text.font.withSize(16)
		}
		
		UIView.animate(withDuration: animationDuration/2) {
			self.text.alpha = 1
		}
	}
}

