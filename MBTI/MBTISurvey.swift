//
//  Questions.swift
//  MBTI
//
//  Created by deadbird on 03/12/2017.
//  Copyright © 2017 deadbird. All rights reserved.
//

import Foundation

struct Question {
	var title:String
	var answer1:Answer
	var answer2:Answer
	var selectedAnswer:SelectedAnswer?
	
	init(title:String, answer1:Answer, answer2:Answer, selectedAnswer:SelectedAnswer? = nil) {
		self.title = title
		self.answer1 = answer1
		self.answer2 = answer2
		self.selectedAnswer = selectedAnswer
	}
}

class Answer {
	init(answerText:String, dimension:Dimensions, pointsIfFemale:UInt8, pointsIfMale:UInt8) {
		self.answerText = answerText
		self.dimension = dimension
		self.pointsIfMale = pointsIfMale
		self.pointsIfFemale = pointsIfFemale
	}
	private(set) var answerText:String
	private(set) var dimension:Dimensions
	private(set) var pointsIfFemale:UInt8
	private(set) var pointsIfMale:UInt8
}

enum Dimensions {
	case E, S, F, P, I, N, T, J, none
}

enum Sex {
	case male, female
}

enum SelectedAnswer {
	case answerA, answerB
}

class MBTISurvey {
	private(set) var questions:[Question] = []
	
	private func addQuestion(question:Question) {
		questions.append(question)
	}
	
	public func selectedAnswer(atIndex:Int, answer:SelectedAnswer) {
		questions[atIndex].selectedAnswer = answer
	}
	
	public func getNumberOfAnsweredQuestions() -> Int {
		var n = 0
		for question in questions {
			if question.selectedAnswer != nil {
				n += 1
			}
		}
		return n
	}
	
	init() {
		//1
		addQuestion(question: Question(
			title: "Habituellement",
			answer1:Answer(answerText: "vous liez-vous facilement ?", dimension: .E, pointsIfFemale: 2, pointsIfMale: 2),
			answer2:Answer(answerText: "êtes-vous plutôt tranquille et réservé ?", dimension: .I, pointsIfFemale: 2, pointsIfMale: 2)
			)
		)
		//2
		addQuestion(question: Question(
			title: "Lorsque vous lisez pour votre plaisir, préférez-vous",
			answer1:Answer(answerText: "les manière nouvelles de dire les choses ?", dimension: .N, pointsIfFemale: 2, pointsIfMale: 2),
			answer2:Answer(answerText: "les écrivains qui disent les choses de façon directe et simple ?", dimension: .S, pointsIfFemale: 0, pointsIfMale: 1)
			)
		)
		//3
		addQuestion(question: Question(
			title: "Laissez-vous le plus souvent",
			answer1:Answer(answerText: "votre cœur guider votre tête ?", dimension: .F, pointsIfFemale: 1, pointsIfMale: 1),
			answer2:Answer(answerText: "votre tête guider votre cœur ?", dimension: .T, pointsIfFemale: 1, pointsIfMale: 1)
			)
		)
		//4
		addQuestion(question: Question(
			title: "Quand vous partez quelque part pour la journée, préférez-vous",
			answer1:Answer(answerText: "planifier ce que vous aller faire et quand ?", dimension: .J, pointsIfFemale: 2, pointsIfMale: 2),
			answer2:Answer(answerText: "y aller tout simplement ?", dimension: .P, pointsIfFemale: 2, pointsIfMale: 2)
			)
		)
		//5
		addQuestion(question: Question(
			title: "Quand vous êtes dans un groupe de personnes, le plus souvent",
			answer1:Answer(answerText: "vous joignez-vous à la conversation ?", dimension: .E, pointsIfFemale: 1, pointsIfMale: 1),
			answer2:Answer(answerText: "parlez-vous à une seule personne à la fois ?", dimension: .I, pointsIfFemale: 2, pointsIfMale: 2)
			)
		)
		//6
		addQuestion(question: Question(
			title: "Préférez-vous",
			answer1:Answer(answerText: "préparer les rendez-vous, réceptions, etc., longtemps à l'avance ?", dimension: .J, pointsIfFemale: 2, pointsIfMale: 2),
			answer2:Answer(answerText: "vous sentir libre de faire ce qui vous plaît le moment venu ?", dimension: .P, pointsIfFemale: 2, pointsIfMale: 2)
			)
		)
		//7
		addQuestion(question: Question(
			title: "Si vous étiez professeur, préféreriez-vous donner",
			answer1:Answer(answerText: "des cours pratiques ?", dimension: .S, pointsIfFemale: 1, pointsIfMale: 0),
			answer2:Answer(answerText: "des cours impliquant des théories ?", dimension: .N, pointsIfFemale: 2, pointsIfMale: 0)
			)
		)
		//8
		addQuestion(question: Question(
			title: "Etes-vous au mieux",
			answer1:Answer(answerText: "quand vous affrontez l'inattendu ?", dimension: .P, pointsIfFemale: 1, pointsIfMale: 2),
			answer2:Answer(answerText: "quand vous suivez un plan soigneusement préparé ?", dimension: .J, pointsIfFemale: 1, pointsIfMale: 1)
			)
		)
		//9
		addQuestion(question: Question(
			title: "Dans les réceptions et soirées",
			answer1:Answer(answerText: "vous ennuyez-vous souvent ?", dimension: .I, pointsIfFemale: 2, pointsIfMale: 2),
			answer2:Answer(answerText: "vous amusez-vous toujours ?", dimension: .E, pointsIfFemale: 2, pointsIfMale: 2)
			)
		)
		//10
		addQuestion(question: Question(
			title: "Vaut-il mieux",
			answer1:Answer(answerText: "ne pas être chaleureux ?", dimension: .T, pointsIfFemale: 2, pointsIfMale: 0),
			answer2:Answer(answerText: "être trop chaleureux ?", dimension: .F, pointsIfFemale: 1, pointsIfMale: 0)
			)
		)
		//11
		addQuestion(question: Question(
			title: "En général:",
			answer1:Answer(answerText: "Parlez-vous facilement à presque n'importe qui aussi longtemps que nécessaire ?", dimension: .E, pointsIfFemale: 1, pointsIfMale: 2),
			answer2:Answer(answerText: "Avez-vous beaucoup à dire uniquement à certaines personnes et dans certaines conditions ?", dimension: .I, pointsIfFemale: 1, pointsIfMale: 1)
			)
		)
		//12
		addQuestion(question: Question(
			title: "Vous entendez-vous habituellement mieux avec",
			answer1:Answer(answerText: "les personnes imaginatives ?", dimension: .N, pointsIfFemale: 2, pointsIfMale: 2),
			answer2:Answer(answerText: "les personnes réalistes ?", dimension: .S, pointsIfFemale: 1, pointsIfMale: 2)
			)
		)
		//13
		addQuestion(question: Question(
			title: "Donnez-vous habituellement le plus d'importance",
			answer1:Answer(answerText: "aux sentiments qu'à la logique ?", dimension: .F, pointsIfFemale: 2, pointsIfMale: 2),
			answer2:Answer(answerText: "à la logique qu'aux sentiments ?", dimension: .S, pointsIfFemale: 1, pointsIfMale: 2)
			)
		)
		//14
		addQuestion(question: Question(
			title: "Est-ce que le fait de suivre un programme",
			answer1:Answer(answerText: "vous attire ?", dimension: .J, pointsIfFemale: 1, pointsIfMale: 1),
			answer2:Answer(answerText: "vous agace ?", dimension: .P, pointsIfFemale: 2, pointsIfMale: 2)
			)
		)
		//15
		addQuestion(question: Question(
			title: "Diriez-vous que vous êtes",
			answer1:Answer(answerText: "plus enthousiaste que la moyenne des gens ?", dimension: .E, pointsIfFemale: 0, pointsIfMale: 0),
			answer2:Answer(answerText: "moins enthousiaste que la moyenne des gens ?", dimension: .I, pointsIfFemale: 2, pointsIfMale: 2)
			)
		)
		//16
		addQuestion(question: Question(
			title: "Quand vous démarrez un projet qui doit être terminé en une semaine",
			answer1:Answer(answerText: "prenez-vous le temps d'établir la liste des choses à faire et l'ordre dans lequel il faut le faire ?", dimension: .J, pointsIfFemale: 1, pointsIfMale: 1),
			answer2:Answer(answerText: "vous plongez-vous immédiatement dans l'action ?", dimension: .P, pointsIfFemale: 2, pointsIfMale: 2)
			)
		)
		//17
		addQuestion(question: Question(
			title: "Admirez-vous les personnes qui sont",
			answer1:Answer(answerText: "sufisamment conventionnelles pour ne pas se faire remarquer", dimension: .S, pointsIfFemale: 2, pointsIfMale: 2),
			answer2:Answer(answerText: "tellement originales et personnelles qu'elles ne se soucient pas de savoir si on les remarque ou pas ?", dimension: .N, pointsIfFemale: 1, pointsIfMale: 0)
			)
		)
		//18
		addQuestion(question: Question(
			title: "Dans votre travail quotidien, faîtes-vous en sorte",
			answer1:Answer(answerText: "que les urgences vous amènent à travailler sous pression ?", dimension: .P, pointsIfFemale: 2, pointsIfMale: 2),
			answer2:Answer(answerText: "de planifier votre travail de manière à éviter de travailler sous pression ?", dimension: .J, pointsIfFemale: 2, pointsIfMale: 2)
			)
		)
		//19
		addQuestion(question: Question(
			title: "Avez-vous tendance à avoir",
			answer1:Answer(answerText: "des amitiés approfondies avec peu de personnes ?", dimension: .I, pointsIfFemale: 0, pointsIfMale: 0),
			answer2:Answer(answerText: "des amitiés étendues à beaucoup de personnes différentes ?", dimension: .E, pointsIfFemale: 2, pointsIfMale: 2)
			)
		)
		//20
		addQuestion(question: Question(
			title: "Les personnes que vous rencontrez pour la première fois peuvent-elles connaître vos centres d'intérêts",
			answer1:Answer(answerText: "immédiatement ?", dimension: .E, pointsIfFemale: 2, pointsIfMale: 2),
			answer2:Answer(answerText: "seulement après qu'elles aient vraiment eu l'occasion de vous fréquenter ?", dimension: .I, pointsIfFemale: 1, pointsIfMale: 0)
			)
		)
		//21
		addQuestion(question: Question(
			title: "Préféreriez-vous avoir comme ami quelqu'un qui a",
			answer1:Answer(answerText: "toujours de nouvelles idées ?", dimension: .N, pointsIfFemale: 2, pointsIfMale: 1),
			answer2:Answer(answerText: "les deux pieds sur terre ?", dimension: .S, pointsIfFemale: 2, pointsIfMale: 2)
			)
		)
		//22
		addQuestion(question: Question(
			title: "Pensez-vous c'est un plus grand défaut d'être",
			answer1:Answer(answerText: "antipathique ?", dimension: .F, pointsIfFemale: 1, pointsIfMale: 1),
			answer2:Answer(answerText: "illogique ?", dimension: .T, pointsIfFemale: 2, pointsIfMale: 2)
			)
		)
		//23
		addQuestion(question: Question(
			title: "Quand vous avez un travail particulier à faire, aimez-vous",
			answer1:Answer(answerText: "l'organiser soigneusement avant de commencer ?", dimension: .J, pointsIfFemale: 1, pointsIfMale: 1),
			answer2:Answer(answerText: "découvrir au fur et à mesure ce qui est nécessaire ?", dimension: .P, pointsIfFemale: 2, pointsIfMale: 2)
			)
		)
		//24
		addQuestion(question: Question(
			title: "Habituellement",
			answer1:Answer(answerText: "montrez-vous vos sentiments librement ?", dimension: .E, pointsIfFemale: 2, pointsIfMale: 2),
			answer2:Answer(answerText: "les gardez-vous pour vous ?", dimension: .I, pointsIfFemale: 1, pointsIfMale: 1)
			)
		)
		//25
		addQuestion(question: Question(
			title: "Quand il est prévu longtemps à l'avance que vous ferez une certaine chose à une certain moment, trouvez-vous",
			answer1:Answer(answerText: "agréable de faire selon le plan prévu", dimension: .J, pointsIfFemale: 0, pointsIfMale: 1),
			answer2:Answer(answerText: "désagréable d'être ainsi lié ?", dimension: .P, pointsIfFemale: 2, pointsIfMale: 2)
			)
		)
		//26
		addQuestion(question: Question(
			title: "Préféreriez-vous être considéré comme une personne",
			answer1:Answer(answerText: "pratique ?", dimension: .S, pointsIfFemale: 2, pointsIfMale: 2),
			answer2:Answer(answerText: "ingénieuse ?", dimension: .N, pointsIfFemale: 2, pointsIfMale: 1)
			)
		)
		//27
		addQuestion(question: Question(
            title: "En général:",
			answer1:Answer(answerText: "Préféreriez-vous faire les choses à la dernière minute ?", dimension: .P, pointsIfFemale: 2, pointsIfMale: 2),
			answer2:Answer(answerText: "Faire les choses à la dernière minute vous porte-t-il sur les nerfs ?", dimension: .J, pointsIfFemale: 2, pointsIfMale: 1)
			)
		)
		//28
		addQuestion(question: Question(
            title: "En général:",
			answer1:Answer(answerText: "Êtes vous l'un des derniers à apprendre ce qui se passe ?", dimension: .I, pointsIfFemale: 1, pointsIfMale: 1),
			answer2:Answer(answerText: "Savez-vous tout sur tout le monde ?", dimension: .E, pointsIfFemale: 2, pointsIfMale: 1)
			)
		)
		//29
		addQuestion(question: Question(
			title: "Choisissez le mot ou l'expression que vous préférez:",
			answer1:Answer(answerText: "faits", dimension: .S, pointsIfFemale: 0, pointsIfMale: 0),
			answer2:Answer(answerText: "théories", dimension: .N, pointsIfFemale: 2, pointsIfMale: 1)
			)
		)
		//30
		addQuestion(question: Question(
			title: "Choisissez le mot ou l'expression que vous préférez:",
			answer1:Answer(answerText: "pensée", dimension: .T, pointsIfFemale: 1, pointsIfMale: 1),
			answer2:Answer(answerText: "sentiment", dimension: .F, pointsIfFemale: 2, pointsIfMale: 1)
			)
		)
		//31
		addQuestion(question: Question(
			title: "Choisissez le mot ou l'expression que vous préférez:",
			answer1:Answer(answerText: "programmé", dimension: .J, pointsIfFemale: 2, pointsIfMale: 2),
			answer2:Answer(answerText: "non planifié", dimension: .P, pointsIfFemale: 2, pointsIfMale: 2)
			)
		)
		//32
		addQuestion(question: Question(
			title: "Choisissez le mot ou l'expression que vous préférez:",
			answer1:Answer(answerText: "hypothèse", dimension: .N, pointsIfFemale: 2, pointsIfMale: 2),
			answer2:Answer(answerText: "certitude", dimension: .S, pointsIfFemale: 1, pointsIfMale: 1)
			)
		)
		//33
		addQuestion(question: Question(
			title: "Choisissez le mot ou l'expression que vous préférez:",
			answer1:Answer(answerText: "gentil", dimension: .F, pointsIfFemale: 2, pointsIfMale: 2),
			answer2:Answer(answerText: "ferme", dimension: .T, pointsIfFemale: 2, pointsIfMale: 2)
			)
		)
		//34
		addQuestion(question: Question(
			title: "Choisissez le mot ou l'expression que vous préférez:",
			answer1:Answer(answerText: "liant", dimension: .E, pointsIfFemale: 0, pointsIfMale: 0),
			answer2:Answer(answerText: "distant", dimension: .I, pointsIfFemale: 2, pointsIfMale: 2)
			)
		)
		//35
		addQuestion(question: Question(
			title: "Choisissez le mot ou l'expression que vous préférez:",
			answer1:Answer(answerText: "affirmation", dimension: .S, pointsIfFemale: 2, pointsIfMale: 1),
			answer2:Answer(answerText: "concept", dimension: .N, pointsIfFemale: 1, pointsIfMale: 1)
			)
		)
		//36
		addQuestion(question: Question(
			title: "Choisissez le mot ou l'expression que vous préférez:",
			answer1:Answer(answerText: "convaincant", dimension: .T, pointsIfFemale: 0, pointsIfMale: 0),
			answer2:Answer(answerText: "touchant", dimension: .F, pointsIfFemale: 2, pointsIfMale: 2)
			)
		)
		//37
		addQuestion(question: Question(
			title: "Choisissez le mot ou l'expression que vous préférez:",
			answer1:Answer(answerText: "ponctuel", dimension: .J, pointsIfFemale: 0, pointsIfMale: 0),
			answer2:Answer(answerText: "pas pressé", dimension: .P, pointsIfFemale: 2, pointsIfMale: 2)
			)
		)
		//38
		addQuestion(question: Question(
			title: "Choisissez le mot ou l'expression que vous préférez:",
			answer1:Answer(answerText: "figuré", dimension: .N, pointsIfFemale: 1, pointsIfMale: 1),
			answer2:Answer(answerText: "littéral", dimension: .S, pointsIfFemale: 1, pointsIfMale: 1)
			)
		)
		//39
		addQuestion(question: Question(
			title: "Choisissez le mot ou l'expression que vous préférez:",
			answer1:Answer(answerText: "sympathiser", dimension: .F, pointsIfFemale: 2, pointsIfMale: 2),
			answer2:Answer(answerText: "analyser", dimension: .T, pointsIfFemale: 1, pointsIfMale: 2)
			)
		)
		//40
		addQuestion(question: Question(
			title: "Choisissez le mot ou l'expression que vous préférez:",
			answer1:Answer(answerText: "loquace", dimension: .E, pointsIfFemale: 2, pointsIfMale: 2),
			answer2:Answer(answerText: "réservé", dimension: .I, pointsIfFemale: 1, pointsIfMale: 1)
			)
		)
		//41
		addQuestion(question: Question(
			title: "Choisissez le mot ou l'expression que vous préférez:",
			answer1:Answer(answerText: "fabriquer", dimension: .S, pointsIfFemale: 2, pointsIfMale: 2),
			answer2:Answer(answerText: "créer", dimension: .N, pointsIfFemale: 0, pointsIfMale: 0)
			)
		)
		//42
		addQuestion(question: Question(
			title: "Choisissez le mot ou l'expression que vous préférez:",
			answer1:Answer(answerText: "justice", dimension: .T, pointsIfFemale: 0, pointsIfMale: 0),
			answer2:Answer(answerText: "indulgence", dimension: .F, pointsIfFemale: 2, pointsIfMale: 2)
			)
		)
		//43
		addQuestion(question: Question(
			title: "Choisissez le mot ou l'expression que vous préférez:",
			answer1:Answer(answerText: "systématique", dimension: .J, pointsIfFemale: 2, pointsIfMale: 2),
			answer2:Answer(answerText: "informel", dimension: .P, pointsIfFemale: 0, pointsIfMale: 0)
			)
		)
		//44
		addQuestion(question: Question(
			title: "Choisissez le mot ou l'expression que vous préférez:",
			answer1:Answer(answerText: "imaginatif", dimension: .N, pointsIfFemale: 2, pointsIfMale: 2),
			answer2:Answer(answerText: "réaliste", dimension: .S, pointsIfFemale: 2, pointsIfMale: 2)
			)
		)
		//45
		addQuestion(question: Question(
			title: "Choisissez le mot ou l'expression que vous préférez:",
			answer1:Answer(answerText: "compassion", dimension: .F, pointsIfFemale: 2, pointsIfMale: 2),
			answer2:Answer(answerText: "prévision", dimension: .T, pointsIfFemale: 0, pointsIfMale: 0)
			)
		)
		//46
		addQuestion(question: Question(
			title: "Choisissez le mot ou l'expression que vous préférez:",
			answer1:Answer(answerText: "animé", dimension: .E, pointsIfFemale: 2, pointsIfMale: 1),
			answer2:Answer(answerText: "calme", dimension: .I, pointsIfFemale: 1, pointsIfMale: 1)
			)
		)
		//47
		addQuestion(question: Question(
			title: "Choisissez le mot ou l'expression que vous préférez:",
			answer1:Answer(answerText: "logique", dimension: .T, pointsIfFemale: 1, pointsIfMale: 2),
			answer2:Answer(answerText: "fascinant", dimension: .F, pointsIfFemale: 1, pointsIfMale: 1)
			)
		)
		//48
		addQuestion(question: Question(
			title: "Choisissez le mot ou l'expression que vous préférez:",
			answer1:Answer(answerText: "profits", dimension: .T, pointsIfFemale: 1, pointsIfMale: 2),
			answer2:Answer(answerText: "bienfaits", dimension: .F, pointsIfFemale: 0, pointsIfMale: 0)
			)
		)
		//49
		addQuestion(question: Question(
			title: "Choisissez le mot ou l'expression que vous préférez:",
			answer1:Answer(answerText: "décision", dimension: .J, pointsIfFemale: 0, pointsIfMale: 1),
			answer2:Answer(answerText: "impulsion", dimension: .P, pointsIfFemale: 2, pointsIfMale: 1)
			)
		)
		//50
		addQuestion(question: Question(
			title: "Choisissez le mot ou l'expression que vous préférez:",
			answer1:Answer(answerText: "élan", dimension: .N, pointsIfFemale: 1, pointsIfMale: 1),
			answer2:Answer(answerText: "stabilité", dimension: .S, pointsIfFemale: 1, pointsIfMale: 1)
			)
		)
		//51
		addQuestion(question: Question(
			title: "Choisissez le mot ou l'expression que vous préférez:",
			answer1:Answer(answerText: "doux", dimension: .F, pointsIfFemale: 0, pointsIfMale: 0),
			answer2:Answer(answerText: "dur", dimension: .T, pointsIfFemale: 2, pointsIfMale: 2)
			)
		)
		//52
		addQuestion(question: Question(
			title: "Choisissez le mot ou l'expression que vous préférez:",
			answer1:Answer(answerText: "parler", dimension: .E, pointsIfFemale: 0, pointsIfMale: 0),
			answer2:Answer(answerText: "rédiger", dimension: .I, pointsIfFemale: 1, pointsIfMale: 2)
			)
		)
		//53
		addQuestion(question: Question(
			title: "Dans un groupe, le plus souvent",
			answer1:Answer(answerText: "faîtes-vous les présentations ?", dimension: .E, pointsIfFemale: 1, pointsIfMale: 1),
			answer2:Answer(answerText: "êtes-vous présenté ?", dimension: .I, pointsIfFemale: 2, pointsIfMale: 2)
			)
		)
		//54
		addQuestion(question: Question(
			title: "Dans votre manière de vivre, préférez-vous",
			answer1:Answer(answerText: "être original ?", dimension: .N, pointsIfFemale: 2, pointsIfMale: 1),
			answer2:Answer(answerText: "être conventionnel ?", dimension: .S, pointsIfFemale: 1, pointsIfMale: 2)
			)
		)
		//55
		addQuestion(question: Question(
			title: "Est-ce un plus grand compliment d'être considéré comme",
			answer1:Answer(answerText: "une personne qui a du cœur ?", dimension: .F, pointsIfFemale: 1, pointsIfMale: 0),
			answer2:Answer(answerText: "une personne toujours raisonnable ?", dimension: .T, pointsIfFemale: 2, pointsIfMale: 2)
			)
		)
		//56
		addQuestion(question: Question(
			title: "Pensez-vous qu'un emploi du temps régulier est",
			answer1:Answer(answerText: "une manière confortable de faire les choses ?", dimension: .J, pointsIfFemale: 1, pointsIfMale: 1),
			answer2:Answer(answerText: "pénible même quand c'est nécessaire ?", dimension: .P, pointsIfFemale: 1, pointsIfMale: 1)
			)
		)
		//57
		addQuestion(question: Question(
			title: "Quand quelque chose commence à être à la mode, êtes-vous d'habitude",
			answer1:Answer(answerText: "l'un des premiers à l'essayer ?", dimension: .E, pointsIfFemale: 0, pointsIfMale: 2),
			answer2:Answer(answerText: "pas vraiment intéressé ?", dimension: .J, pointsIfFemale: 0, pointsIfMale: 0)
			)
		)
		//58
		addQuestion(question: Question(
			title: "L'idée de faire un programme détaillé de votre week-end",
			answer1:Answer(answerText: "vous attire ?", dimension: .J, pointsIfFemale: 2, pointsIfMale: 2),
			answer2:Answer(answerText: "vous agace ?", dimension: .P, pointsIfFemale: 1, pointsIfMale: 2)
			)
		)
		//59
		addQuestion(question: Question(
			title: "Lorsque vous faites quelques chose que beaucoup d'autres personnes font, qu'est-ce qui vous attire le plus ?",
			answer1:Answer(answerText: "suivre une procédure connue.", dimension: .S, pointsIfFemale: 2, pointsIfMale: 2),
			answer2:Answer(answerText: "inventer une méthode personnelle.", dimension: .N, pointsIfFemale: 1, pointsIfMale: 0)
			)
		)
		//60
		addQuestion(question: Question(
			title: "Il vous est plus difficile de vous adapter à",
			answer1:Answer(answerText: "un programme régulier ?", dimension: .P, pointsIfFemale: 1, pointsIfMale: 1),
			answer2:Answer(answerText: "des changements fréquents ?", dimension: .J, pointsIfFemale: 1, pointsIfMale: 1)
			)
		)
		//61
		addQuestion(question: Question(
			title: "Êtes-vous",
			answer1:Answer(answerText: "facile à connaître ?", dimension: .E, pointsIfFemale: 2, pointsIfMale: 2),
			answer2:Answer(answerText: "difficile à connaître ?", dimension: .I, pointsIfFemale: 1, pointsIfMale: 1)
			)
		)
		//62
		addQuestion(question: Question(
			title: "Est-ce pour vous un plus grand compliment de dire de quelqu'un qu'il a",
			answer1:Answer(answerText: "de l'imagination ?", dimension: .N, pointsIfFemale: 2, pointsIfMale: 2),
			answer2:Answer(answerText: "du bon sens ?", dimension: .S, pointsIfFemale: 1, pointsIfMale: 2)
			)
		)
		//63
		addQuestion(question: Question(
			title: "Êtes-vous plus soucieux",
			answer1:Answer(answerText: "des sentiments des gens ?", dimension: .F, pointsIfFemale: 1, pointsIfMale: 1),
			answer2:Answer(answerText: "de leurs droits ?", dimension: .T, pointsIfFemale: 2, pointsIfMale: 2)
			)
		)
		//64
		addQuestion(question: Question(
			title: "Si vous êtes dans une situation embarassante, habituellement",
			answer1:Answer(answerText: "vous en tirez-vous par une plaisanterie ?", dimension: .E, pointsIfFemale: 1, pointsIfMale: 1),
			answer2:Answer(answerText: "pensez-vous plusieurs jours plus tard à ce que vous auriez dû dire ?", dimension: .I, pointsIfFemale: 1, pointsIfMale: 1)
			)
		)
		//65
		addQuestion(question: Question(
			title: "En accomplissant un travail, préférez-vous",
			answer1:Answer(answerText: "démarrer le plus tôt possible de façon à finir avec du temps libre ?", dimension: .J, pointsIfFemale: 1, pointsIfMale: 1),
			answer2:Answer(answerText: "donner un coup de collier à la dernière minute ?", dimension: .P, pointsIfFemale: 2, pointsIfMale: 2)
			)
		)
		//66
		addQuestion(question: Question(
			title: "Face à un problème non résolu, préférez-vous",
			answer1:Answer(answerText: "suivre des méthodes déjà éprouvées ?", dimension: .S, pointsIfFemale: 2, pointsIfMale: 2),
			answer2:Answer(answerText: "analyser puis expérimenter une nouvelle démarche ?", dimension: .N, pointsIfFemale: 1, pointsIfMale: 0)
			)
		)
		//67
		addQuestion(question: Question(
			title: "Quand vous pensez à une chose mineure ou à un petit achat à effectuer",
			answer1:Answer(answerText: "ne vous en rappelez-vous qu'au dernier moment ?", dimension: .P, pointsIfFemale: 1, pointsIfMale: 1),
			answer2:Answer(answerText: "le notez-vous sur un papier pour vous en rappeler ?", dimension: .J, pointsIfFemale: 1, pointsIfMale: 1)
			)
		)
		//68
		addQuestion(question: Question(
			title: "Pensez-vous que les personnes proches de vous savent ce que vous pensez  et ressentez",
			answer1:Answer(answerText: "à propos de la plupart des choses ?", dimension: .E, pointsIfFemale: 1, pointsIfMale: 1),
			answer2:Answer(answerText: "uniquement quand vous avez une bonne raison de le leur dire ?", dimension: .I, pointsIfFemale: 1, pointsIfMale: 1)
			)
		)
		//69
		addQuestion(question: Question(
			title: "Préférez-vous",
			answer1:Answer(answerText: "une occasion qui vous conduit à de plus grandes choses ?", dimension: .N, pointsIfFemale: 1, pointsIfMale: 0),
			answer2:Answer(answerText: "une expérience que vous êtes sûr d'aimer ?", dimension: .S, pointsIfFemale: 2, pointsIfMale: 0)
			)
		)
		//70
		addQuestion(question: Question(
			title: "En décidant quelque chose d'important",
			answer1:Answer(answerText: "estimez-vous pouvoir faire confiance à vos sentiments pour savoir ce qui est mieux ?", dimension: .F, pointsIfFemale: 2, pointsIfMale: 0),
			answer2:Answer(answerText: "pensez-vous devoir vous appuyer sur la logique, quels que soient vos sentiments ?", dimension: .T, pointsIfFemale: 1, pointsIfMale: 0)
			)
		)
		//71
		addQuestion(question: Question(
			title: "Un samedi matin",
			answer1:Answer(answerText: "seriez-vous capable de dire assez précisément ce que vous allez faire ?", dimension: .J, pointsIfFemale: 1, pointsIfMale: 2),
			answer2:Answer(answerText: "allez-vous attendre le dernier moment pour choisir entre de multiples possibilités ?", dimension: .P, pointsIfFemale: 1, pointsIfMale: 1)
			)
		)
		//72
		addQuestion(question: Question(
			title: "Quand vous êtes à une réception ou une soirée",
			answer1:Answer(answerText: "aimez-vous jouer les bout-en-train ?", dimension: .E, pointsIfFemale: 2, pointsIfMale: 2),
			answer2:Answer(answerText: "préférez-vous laisser les autres s'amuser comme ils l'entendent ?", dimension: .I, pointsIfFemale: 1, pointsIfMale: 0)
			)
		)
		//73
		addQuestion(question: Question(
			title: "Choisissez le mot ou l'expression que vous préférez:",
			answer1:Answer(answerText: "Concret", dimension: .S, pointsIfFemale: 0, pointsIfMale: 0),
			answer2:Answer(answerText: "Abstrait", dimension: .N, pointsIfFemale: 2, pointsIfMale: 2)
			)
		)
		//74
		addQuestion(question: Question(
			title: "Choisissez le mot ou l'expression que vous préférez:",
			answer1:Answer(answerText: "Ferme d'esprit", dimension: .T, pointsIfFemale: 2, pointsIfMale: 2),
			answer2:Answer(answerText: "Chaleureux", dimension: .F, pointsIfFemale: 1, pointsIfMale: 1)
			)
		)
		//75
		addQuestion(question: Question(
			title: "Choisissez le mot ou l'expression que vous préférez:",
			answer1:Answer(answerText: "Systématique", dimension: .J, pointsIfFemale: 2, pointsIfMale: 0),
			answer2:Answer(answerText: "Spontané", dimension: .P, pointsIfFemale: 0, pointsIfMale: 0)
			)
		)
		//76
		addQuestion(question: Question(
			title: "Choisissez le mot ou l'expression que vous préférez:",
			answer1:Answer(answerText: "Inventer", dimension: .N, pointsIfFemale: 2, pointsIfMale: 2),
			answer2:Answer(answerText: "Construire", dimension: .S, pointsIfFemale: 2, pointsIfMale: 1)
			)
		)
		//77
		addQuestion(question: Question(
			title: "Choisissez le mot ou l'expression que vous préférez:",
			answer1:Answer(answerText: "Conciliateur", dimension: .F, pointsIfFemale: 0, pointsIfMale: 0),
			answer2:Answer(answerText: "Juge", dimension: .T, pointsIfFemale: 2, pointsIfMale: 2)
			)
		)
		//78
		addQuestion(question: Question(
			title: "Choisissez le mot ou l'expression que vous préférez:",
			answer1:Answer(answerText: "Cordial", dimension: .E, pointsIfFemale: 0, pointsIfMale: 0),
			answer2:Answer(answerText: "Réservé", dimension: .I, pointsIfFemale: 2, pointsIfMale: 2)
			)
		)
		//79
		addQuestion(question: Question(
			title: "Choisissez le mot ou l'expression que vous préférez:",
			answer1:Answer(answerText: "Production", dimension: .S, pointsIfFemale: 2, pointsIfMale: 2),
			answer2:Answer(answerText: "Conception", dimension: .N, pointsIfFemale: 0, pointsIfMale: 1)
			)
		)
		//80
		addQuestion(question: Question(
			title: "Choisissez le mot ou l'expression que vous préférez:",
			answer1:Answer(answerText: "Déterminé", dimension: .T, pointsIfFemale: 0, pointsIfMale: 0),
			answer2:Answer(answerText: "Dévoué", dimension: .F, pointsIfFemale: 2, pointsIfMale: 2)
			)
		)
		//81
		addQuestion(question: Question(
			title: "Choisissez le mot ou l'expression que vous préférez:",
			answer1:Answer(answerText: "Permanent", dimension: .J, pointsIfFemale: 1, pointsIfMale: 1),
			answer2:Answer(answerText: "Changeant", dimension: .P, pointsIfFemale: 1, pointsIfMale: 0)
			)
		)
		//82
		addQuestion(question: Question(
			title: "Choisissez le mot ou l'expression que vous préférez:",
			answer1:Answer(answerText: "Modifier", dimension: .N, pointsIfFemale: 0, pointsIfMale: 1),
			answer2:Answer(answerText: "Conserver", dimension: .S, pointsIfFemale: 2, pointsIfMale: 2)
			)
		)
		//83
		addQuestion(question: Question(
			title: "Choisissez le mot ou l'expression que vous préférez:",
			answer1:Answer(answerText: "Se mettre d'accord", dimension: .F, pointsIfFemale: 0, pointsIfMale: 2),
			answer2:Answer(answerText: "Argumenter", dimension: .T, pointsIfFemale: 0, pointsIfMale: 1)
			)
		)
		//84
		addQuestion(question: Question(
			title: "Choisissez le mot ou l'expression que vous préférez:",
			answer1:Answer(answerText: "Faire la fête", dimension: .E, pointsIfFemale: 0, pointsIfMale: 1),
			answer2:Answer(answerText: "Aller au théâtre", dimension: .I, pointsIfFemale: 1, pointsIfMale: 1)
			)
		)
		//85
		addQuestion(question: Question(
			title: "Choisissez le mot ou l'expression que vous préférez:",
			answer1:Answer(answerText: "Connu", dimension: .S, pointsIfFemale: 1, pointsIfMale: 1),
			answer2:Answer(answerText: "Inconnu", dimension: .N, pointsIfFemale: 2, pointsIfMale: 2)
			)
		)
		//86
		addQuestion(question: Question(
			title: "Choisissez le mot ou l'expression que vous préférez:",
			answer1:Answer(answerText: "Méfiant", dimension: .T, pointsIfFemale: 0, pointsIfMale: 2),
			answer2:Answer(answerText: "Confiant", dimension: .F, pointsIfFemale: 0, pointsIfMale: 0)
			)
		)
		//87
		addQuestion(question: Question(
			title: "Choisissez le mot ou l'expression que vous préférez:",
			answer1:Answer(answerText: "Ordonné", dimension: .J, pointsIfFemale: 2, pointsIfMale: 1),
			answer2:Answer(answerText: "Décontracté", dimension: .P, pointsIfFemale: 1, pointsIfMale: 0)
			)
		)
		//88
		addQuestion(question: Question(
			title: "Choisissez le mot ou l'expression que vous préférez:",
			answer1:Answer(answerText: "Pas porté à la critique", dimension: .F, pointsIfFemale: 0, pointsIfMale: 1),
			answer2:Answer(answerText: "Porté à la critique", dimension: .T, pointsIfFemale: 0, pointsIfMale: 0)
			)
		)
		//89
		addQuestion(question: Question(
			title: "Choisissez le mot ou l'expression que vous préférez:",
			answer1:Answer(answerText: "Supporter", dimension: .T, pointsIfFemale: 1, pointsIfMale: 2),
			answer2:Answer(answerText: "Pardonner", dimension: .F, pointsIfFemale: 0, pointsIfMale: 0)
			)
		)
		
	}
}


