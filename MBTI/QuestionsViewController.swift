//
//  QuestionsViewController.swift
//  MBTI
//
//  Created by deadbird on 29/12/2017.
//  Copyright © 2017 deadbird. All rights reserved.
//

import UIKit

class QuestionsViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
	
	@IBOutlet weak var progressBar: UIProgressView!
	@IBOutlet weak var questionsList: UITableView!
	@IBOutlet weak var goToResultsButton: UIButton!
	@IBOutlet weak var progressionLabel: UILabel!
	
	private var survey = MBTISurvey()
	private var ratioOfAnsweredQuestions:Float = 0.0 {
		didSet {
			progressBar.progress = ratioOfAnsweredQuestions
			print("ratioOfAnsweredQuestions = \(ratioOfAnsweredQuestions)")
			
			if ratioOfAnsweredQuestions == 1.0 {
				makeResultsAvailable()
			}
		}
	}
	
	override func viewWillAppear(_ animated: Bool) {
		goToResultsButton.alpha = 0
        ratioOfAnsweredQuestions = 0
	}
	
	override func viewDidLoad() {
		super.viewDidLoad()
		
		questionsList.delegate = self
		questionsList.dataSource = self
		
		questionsList.rowHeight = UITableViewAutomaticDimension
		questionsList.estimatedRowHeight = 150
	}
	
	override var prefersStatusBarHidden: Bool {
		return true
	}
	
	func makeResultsAvailable() {
		UIView.animate(withDuration: 2) {
			self.progressionLabel.alpha = 0
			self.progressBar.alpha = 0
			self.goToResultsButton.alpha = 1
		}
	}
	
	func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		return survey.questions.count
	}
	
	func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		let cell = tableView.dequeueReusableCell(withIdentifier: "questionCell", for: indexPath) as! QuestionTableViewCell
		let index = indexPath.item
		
		cell.cellIndex = index
        cell.updateSelection(selectedAnswer: survey.questions[index].selectedAnswer)
		cell.questionTitle.text = survey.questions[index].title
		cell.answer1Button.setTitle(survey.questions[index].answer1.answerText, for: .normal)
		cell.answer2Button.setTitle(survey.questions[index].answer2.answerText, for: .normal)
		
		return cell
	}
	
	func updateRatioOfAnsweredQuestions() {
		ratioOfAnsweredQuestions = Float(survey.getNumberOfAnsweredQuestions()) / Float(survey.questions.count)
	}

	@IBAction func answer1Touched(_ sender: QuestionButton) {
		let questionIndex = sender.questionIndex
		survey.selectedAnswer(atIndex: questionIndex, answer: .answerA)
		
		updateRatioOfAnsweredQuestions()
	}
	
	@IBAction func answer2Touched(_ sender: QuestionButton) {
		let questionIndex = sender.questionIndex
		survey.selectedAnswer(atIndex: questionIndex, answer: .answerB)

		updateRatioOfAnsweredQuestions()
	}
}
